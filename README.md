# lemmymap

**a lemmy federation map**

inspired by [zigbee2mqtt-networkmap](https://github.com/azuwis/zigbee2mqtt-networkmap) based on [vue-d3-network](https://github.com/emiliorizzo/vue-d3-network/)
stats are crawled with [lemmy-stats-crawler](https://github.com/LemmyNet/lemmy-stats-crawler)

https://join-lemmy.org/

## Project setup

```

npm install

```

### Compiles and hot-reloads for development

```

npm run serve

```

### Compiles and minifies for production

```

npm run build

```

### Lints and fixes files

```

npm run lint

```
